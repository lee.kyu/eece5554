#!/usr/bin/env python

###################################################################
# EECE5554 - LAB1
# Kyungsun Lee / Student ID: 001537130 / lee.kyu@northeastern.edu)
###################################################################

import rospy
from gnss.msg import gnss_msg

def callback(data):
    rospy.loginfo("======= BEGIN =======")
    rospy.loginfo(data)

def subscriber():
    rospy.init_node('subscriber')
    rospy.Subscriber("GPSMsg", gnss_msg, callback)
    rospy.loginfo("STart! subscribe!")
    rospy.spin()


if __name__ == '__main__':
    subscriber()
