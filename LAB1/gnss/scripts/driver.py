#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import serial
import utm
from math import sin, pi
from std_msgs.msg import Float64
from gnss.msg import gnss_msg

if __name__ == '__main__':
    # Initialization (GPS_driver node, sensor port information)
    rospy.init_node('GPS_driver')
    serial_port = rospy.get_param('~port','/dev/ttyUSB0')
    serial_baud = rospy.get_param('~baudrate',4800)
    sampling_rate = rospy.get_param('~sampling_rate',5.0)
    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    rospy.logdebug("Initializing sensor with *0100P4\\r\\n ...")
    sampling_count = int(round(1/(sampling_rate*0.007913)))
    rospy.sleep(0.2)
    line = port.readline()
    
    # publish GPSMsg 
    pub = rospy.Publisher('GPSMsg', gnss_msg, queue_size = 10)
    msg = gnss_msg()
    
    sleep_time = 1/sampling_rate - 0.025
   
    try:
        while not rospy.is_shutdown():
            line = port.readline()
            line = str(line, 'UTF-8')
            if line == '':
                rospy.loginfo("Sensor: No data")
            else:
                if line.startswith('$GPGGA'):
                    try: sensorData = line[7:].split(',')
                    except:
                        rospy.loginfo("Data exception: "+line)
                        continue
                    rospy.loginfo(sensorData)

                    #latitude = 0 if sensorData[1] == '' else float(sensorData[1])/100
                    #longitude = 0 if sensorData[3] == '' else float(sensorData[3])/100*(-1)
                    altitude = 0 if sensorData[8] == '' else float(sensorData[8])
                    latitude_dd = int(sensorData[1][:2])
                    latitude_mm = float(sensorData[1][2:]) / 60
                    longitude_ddd = sensorData[3][:3]
                    longitude_mm = float(sensorData[3][3:]) / 60
                
                    print(latitude_dd, latitude_mm, longitude_ddd, longitude_mm)

                    

                    if longitude_ddd[0] == '0':
                        longitude_ddd = int(longitude_ddd[1:3]) * (-1)
                        longitude_mm = longitude_mm * (-1)
                    else:
                        longitude_ddd = int(longitude_ddd[0:3])

                    print(longitude_ddd)

                    latitude = latitude_dd + latitude_mm
                    longitude = longitude_ddd + longitude_mm

                    UTMdata = utm.from_latlon(latitude, longitude)
                    rospy.loginfo(UTMdata)

                    msg.GPS_HEADER="[-----GPS DATA-----]"
                    msg.GPS_LATITUDE = latitude
                    msg.GPS_LONGITUDE = longitude
                    msg.GPS_ALTITUDE = altitude
                    msg.GPS_EASTING = UTMdata[0]
                    msg.GPS_NORTHING = UTMdata[1]
                    msg.GPS_ZONE_NUMBER = UTMdata[2]
                    msg.GPS_ZONE_LETTER = UTMdata[3]
                    pub.publish(msg)
            rospy.sleep(sleep_time)
            
    except rospy.ROSInterruptException:
        port.close()
    
    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down GPS_driver node...")


