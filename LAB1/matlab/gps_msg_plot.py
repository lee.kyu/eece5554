###################################################################
# EECE5554 - LAB1
# Kyungsun Lee / Student ID: 001537130 / lee.kyu@northeastern.edu)
###################################################################

import bagpy
from bagpy import bagreader
import pandas as pd
import seaborn as sea
import matplotlib.pyplot as plt
import numpy as np

# Standing Data
bag_walking = bagreader('walking.bag')
bag_walking.topic_table
data_walking = bag_walking.message_by_topic('GPSMsg')
csv_data_walking = pd.read_csv(data_walking)

# Walking Data
bag_standing = bagreader('standing.bag')
bag_standing.topic_table
data = bag_standing.message_by_topic('GPSMsg')
csv_data_standing = pd.read_csv(data)

fig, ax = bagpy.create_fig(1)

'''
ax[0].scatter(x='Time', y='GPS_ALTITUDE',
              data=csv_data_standing, s=1, label='[Standing] GPS_ALTITUDE/Time')
ax[0].legend()
ax[0].set_xlabel('Time')
ax[0].set_ylabel('GPS_ALTITUDE')
'''
#ax[0].scatter(327985.75353867625, 4689505.73046252, s=5, c='r')


ax[0].scatter(x='Time', y='GPS_ALTITUDE',
              data=csv_data_walking, s=1, label='[Walking] GPS_ALTITUDE/Time')
ax[0].legend()
ax[0].set_xlabel('Time')
ax[0].set_ylabel('GPS_ALTITUDE')


# plt.axis([327900, 328100, 4689400, 4689600])
# plt.xlim([330530, 330532])
# plt.ylim([4674391, 4674422])


# mean, range, deviation... error source(building)
# when you are stationary how does it vary? 


print("[Walking] What's maximum value of GPS_EASTING? ")
print(csv_data_walking["GPS_EASTING"].max())

print("[Walking] What's minimum value of GPS_EASTING? ")
print(csv_data_walking["GPS_EASTING"].min())

print("[Walking] What's maximum value of GPS_NORTHING? ")
print(csv_data_walking["GPS_NORTHING"].max())

print("[Walking] What's minimum value of GPS_NORTHING? ")
print(csv_data_walking["GPS_NORTHING"].min())

print("[Walking] What's mean value of GPS_EASTING? ")
print(csv_data_walking["GPS_EASTING"].mean())

print("[Walking] What's the standard deviation of GPS_EASTING? ")
print(csv_data_walking["GPS_EASTING"].std())

print("[Walking] What's mean value of GPS_NORTHING? ")
print(csv_data_walking["GPS_NORTHING"].mean())

print("[Walking] What's the standard deviation of GPS_NORTHING? ")
print(csv_data_walking["GPS_NORTHING"].std())

print("[Walking] What's maximum value of GPS_ALTITUDE? ")
print(csv_data_walking["GPS_ALTITUDE"].max())

print("[Walking] What's minimum value of GPS_ALTITUDE? ")
print(csv_data_walking["GPS_ALTITUDE"].min())

print("[Walking] What's mean value of GPS_ALTITUDE? ")
print(csv_data_walking["GPS_ALTITUDE"].mean())

print("[Walking] What's the standard deviation of GPS_ALTITUDE? ")
print(csv_data_walking["GPS_ALTITUDE"].std())

plt.show()


'''
[Standing] What's maximum value of GPS_ALTITUDE? 
20.0
[Standing] What's minimum value of GPS_ALTITUDE? 
14.0
[Standing] What's mean value of GPS_ALTITUDE? 
16.250877216101834
[Standing] What's the standard deviation of GPS_ALTITUDE? 
1.714347938353591
[Standing] What's maximum value of GPS_EASTING? 
327986.21875
[Standing] What's minimum value of GPS_EASTING? 
327985.28125
[Standing] What's maximum value of GPS_NORTHING? 
4689506.5
[Standing] What's minimum value of GPS_NORTHING? 
4689505.0
[Standing] What's mean value of GPS_EASTING? 
327985.75353867625
[Standing] What's the standard deviation of GPS_EASTING? 
0.26254872740986346
[Standing] What's mean value of GPS_NORTHING? 
4689505.73046252
[Standing] What's the standard deviation of GPS_NORTHING? 
0.5435479320388146
[Standing] What's maximum value of GPS_ALTITUDE? 
20.0
[Standing] What's minimum value of GPS_ALTITUDE? 
14.0
[Standing] What's mean value of GPS_ALTITUDE? 
16.250877216101834
[Standing] What's the standard deviation of GPS_ALTITUDE? 
1.714347938353591
'''


'''
[Walking] What's maximum value of GPS_EASTING? 
328295.09375
[Walking] What's minimum value of GPS_EASTING? 
328073.71875
[Walking] What's maximum value of GPS_NORTHING? 
4689652.0
[Walking] What's minimum value of GPS_NORTHING? 
4689435.0
[Walking] What's mean value of GPS_EASTING? 
328183.7425547002
[Walking] What's the standard deviation of GPS_EASTING? 
64.13026994741527
[Walking] What's mean value of GPS_NORTHING? 
4689550.3273906
[Walking] What's the standard deviation of GPS_NORTHING? 
61.59424026321011
[Walking] What's maximum value of GPS_ALTITUDE? 
58.0
[Walking] What's minimum value of GPS_ALTITUDE? 
15.199999809265137
[Walking] What's mean value of GPS_ALTITUDE? 
21.823825018525703
[Walking] What's the standard deviation of GPS_ALTITUDE? 
9.505031144909433
'''
