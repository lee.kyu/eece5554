#include <ros/ros.h>
#include <test_topic/TestMsg.h>

int main(int argc, char** argv)
{
	ros::init(argc, argv, "publisher_node");
	ros::NodeHandle nodeHandle;

	ros::Rate rate(1); // loop_rate 1Hz

	ros::Publisher publisher = nodeHandle.advertise<test_topic::TestMsg>("message", 1); // publish topic which is named 'message' and set queue size to 1.
	
	test_topic::TestMsg message;

	while(ros::ok())
	{
		message.var_1 = 12.345;
		message.var_2 = 234.56;
		message.var_3 = 3456.7;

		publisher.publish(message);
		rate.sleep();
	}

	return 0;
}
