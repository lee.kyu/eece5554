#include <ros/ros.h>
#include <test_topic/TestMsg.h>

void messageCallBack(const test_topic::TestMsg::ConstPtr& message) 
{
	ROS_INFO("var_1 : %f", message->var_1);
	ROS_INFO("var_2 : %f", message->var_2);
	ROS_INFO("var_3 : %f", message->var_3);
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "subscriber_node"); // Initialize ros node.
	ros::NodeHandle nodeHandle;

	ros::Rate rate(1); // loop_rate 1Hz.

	ros::Subscriber subscriber = nodeHandle.subscribe<test_topic::TestMsg>("message", 1, messageCallBack); // Subscribe message topic.
	test_topic::TestMsg message;

	while(ros::ok())
	{
		ros::spinOnce();
		rate.sleep();	
	}

	return 0;
}
