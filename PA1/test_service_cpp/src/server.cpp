#include <ros/ros.h>
#include <test_service/AddTwoInts.h>

bool add(test_service::AddTwoInts::Request &request, 
		test_service::AddTwoInts::Response &response)
{
	response.sum = request.a + request.b;
	ROS_INFO("<--- REQUEST x = %ld, y=%ld", (long int)request.a, (long int)request.b);
	ROS_INFO("---> RESPONSE SUM = %ld", (long int)response.sum);
	return true;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "server");
	ros::NodeHandle nodeHandle;

	ros::ServiceServer service = nodeHandle.advertiseService("add_two_ints", add);
	ROS_INFO("READY !!!");
	ros::spin();

	return 0;
}
