#!/usr/bin/env python

from __future__ import print_function

from service_test_python.srv import AddTwoInts,AddTwoIntsResponse
import rospy

def handle_add_two_ints(req):
    print("Returning [%s + %s = %s]"%(req.var_1, req.var_2, (req.var_1 + req.var_2)))
    return AddTwoIntsResponse(req.var_1 + req.var_2)

def add_two_ints_server():
    rospy.init_node('add_two_ints_server')
    s = rospy.Service('add_two_ints', AddTwoInts, handle_add_two_ints)
    print("Ready to add two ints.")
    rospy.spin()

if __name__ == "__main__":
    add_two_ints_server()
