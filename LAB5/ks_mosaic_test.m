% ========================================================================
% Kyungsun's Test Code
% ========================================================================
% % Load images.
% buildingScene = imageDatastore({'latino1.JPG','latino2.JPG','latino3.JPG','latino4.JPG','latino5.JPG','latino6.JPG'});
% 
% % Display images to be stitched.
% montage(buildingScene.Files)
% 
% % Read the first image from the image set.
% I = readimage(buildingScene,1);
% grayImage = rgb2gray(I);
% [y,x,m] = harris(grayImage,1000,'tile',[2 2],'disp');
% 
% I = readimage(buildingScene,2);
% grayImage = rgb2gray(I);
% [y,x,m] = harris(grayImage,1000,'tile',[2 2],'disp');
% 
% I = readimage(buildingScene,3);
% grayImage = rgb2gray(I);
% [y,x,m] = harris(grayImage,1000,'tile',[2 2],'disp');
% 
% I = readimage(buildingScene,4);
% grayImage = rgb2gray(I);
% [y,x,m] = harris(grayImage,1000,'tile',[2 2],'disp');
% 
% I = readimage(buildingScene,5);
% grayImage = rgb2gray(I);
% [y,x,m] = harris(grayImage,1000,'tile',[2 2],'disp');
% 
% I = readimage(buildingScene,6);
% grayImage = rgb2gray(I);
% [y,x,m] = harris(grayImage,1000,'tile',[2 2],'disp');




% ========================================================================
% Harris + Matlab Sample Code
% ========================================================================
% Load images (before/after camera calibration).
% imds = imageDatastore({'latino1.JPG','latino2.JPG','latino3.JPG','latino4.JPG','latino5.JPG','latino6.JPG'});
%imds = imageDatastore({'latino1_rect.jpg','latino2_rect.jpg','latino3_rect.jpg','latino4_rect.jpg','latino5_rect.jpg','latino6_rect.jpg'});
imds = imageDatastore({'cinder_50_1.JPG','cinder_50_2.JPG','cinder_50_3.JPG','cinder_50_4.JPG','cinder_50_5.JPG','cinder_50_6.JPG'});
Img = readimage(imds,1);
grayImage_2 = rgb2gray(Img);
% harris
points = detectSURFFeatures(grayImage_2);
sz = size(points.Location)
[y,x,m] = harris(grayImage_2,points.Count,'tile',[1 1],'disp');
points.Location = [x, y];
points.Metric = m;
[features, points] = extractFeatures(grayImage_2,points);
numImages = numel(imds.Files);
tforms(numImages) = projective2d(eye(3));
% Initialize variable to hold image sizes.
imageSize = zeros(numImages,2);
% Iterate over remaining image pairs
for n = 2:numImages
    % Store points and features for I(n-1).
    pointsPrevious = points;
    featuresPrevious = features;
    % Read I(n).
    I = readimage(imds, n);
    % Convert image to grayscale.
    grayImage = rgb2gray(I);
    % Save image size.
    imageSize(n,:) = size(grayImage);
    
    points = detectSURFFeatures(grayImage);
    [y,x,m] = harris(grayImage,points.Count,'tile',[1 1],'disp');
    points.Location = [x, y];
    points.Metric = m;
    [features, points] = extractFeatures(grayImage,points);
    % Find correspondences between I(n) and I(n-1).
    indexPairs = matchFeatures(features, featuresPrevious, 'Unique', true);
    matchedPoints = points(indexPairs(:,1), :);
    matchedPointsPrev = pointsPrevious(indexPairs(:,2), :);
    % Estimate the transformation between I(n) and I(n-1).
    tforms(n) = estimateGeometricTransform2D(matchedPoints, matchedPointsPrev,...
        'projective', 'Confidence', 99.9, 'MaxNumTrials', 2000);
    % Compute T(n) * T(n-1) * ... * T(1)
    tforms(n).T = tforms(n).T * tforms(n-1).T;
end
% Compute the output limits for each transform.
for i = 1:numel(tforms)
    [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(i,2)], [1 imageSize(i,1)]);
end
avgXLim = mean(xlim, 2);
[~,idx] = sort(avgXLim);
centerIdx = floor((numel(tforms)+1)/2);
centerImageIdx = idx(centerIdx);
Tinv = invert(tforms(centerImageIdx));
for i = 1:numel(tforms)
    tforms(i).T = tforms(i).T * Tinv.T;
end
for i = 1:numel(tforms)
    [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(i,2)], [1 imageSize(i,1)]);
end
maxImageSize = max(imageSize);
% Find the minimum and maximum output limits.
xMin = min([1; xlim(:)]);
xMax = max([maxImageSize(2); xlim(:)]);
yMin = min([1; ylim(:)]);
yMax = max([maxImageSize(1); ylim(:)]);
% Width and height of panorama.
width  = round(xMax - xMin);
height = round(yMax - yMin);
% Initialize the "empty" panorama.
panorama = zeros([height width 3], 'like', I);
blender = vision.AlphaBlender('Operation', 'Binary mask', ...
    'MaskSource', 'Input port');
% Create a 2-D spatial reference object defining the size of the panorama.
xLimits = [xMin xMax];
yLimits = [yMin yMax];
panoramaView = imref2d([height width], xLimits, yLimits);
% Create the panorama.
for i = 1:numImages
    I = readimage(imds, i);
    % Transform I into the panorama.
    warpedImage = imwarp(I, tforms(i), 'OutputView', panoramaView);
    % Generate a binary mask.
    mask = imwarp(true(size(I,1),size(I,2)), tforms(i), 'OutputView', panoramaView);
    % Overlay the warpedImage onto the panorama.
    panorama = step(blender, panorama, warpedImage, mask);
end
figure
imshow(panorama)
