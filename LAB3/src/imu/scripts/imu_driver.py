#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sqlite3 import Timestamp
from time import time
from numpy import float32, float64
import rospy
import serial
from math import sin, cos, pi
#from std_msgs.msg import String
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField

if __name__ == '__main__':
    pub1 = rospy.Publisher('imu_info', Imu, queue_size=10)
    pub2 = rospy.Publisher('mag_info', MagneticField, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(40) # 40hz
    serial_port = rospy.get_param('~port','/dev/ttyUSB0')
    serial_baud = rospy.get_param('~baudrate',115200)
    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    rospy.loginfo("Initialization complete")
    port.write('$VNWRG,07,40*XX'.encode())  # cmd from 01 to 00 to sample continuously  
    imu = Imu()
    mag = MagneticField()

    try:
        while not rospy.is_shutdown():
            imu_str = port.readline()
            #rospy.loginfo(imu_str)
            line = str(imu_str, 'UTF-8')
            if line.startswith('$VNYMR'):
                #rospy.loginfo("Find: "+line)
                try: sensor_data = line[7:].split(',')
                except:
                    rospy.loginfo("Data exception: "+line)
                    continue

                Yaw = sensor_data[0] #Yaw
                Pitch = sensor_data[1] #Pitch
                Roll = sensor_data[2] #Roll
                
                try:
                    cy = cos(float(Yaw) * 0.5)
                    sy = sin(float(Yaw) * 0.5)
                    cp = cos(float(Pitch) * 0.5)
                    sp = sin(float(Pitch) * 0.5)
                    cr = cos(float(Roll) * 0.5)
                    sr = sin(float(Roll) * 0.5)
                except:
                    rospy.loginfo("Data Wrong: "+line)
                    continue

                imu.header.seq = 0
                imu.header.stamp = rospy.Time.now()
                imu.header.frame_id = 'IMU'
                imu.orientation.w = cr * cp * cy + sr * sp * sy
                imu.orientation.x = sr * cp * cy - cr * sp * sy
                imu.orientation.y = cr * sp * cy + sr * cp * sy
                imu.orientation.z = cr * cp * sy - sr * sp * cy

                imu.orientation_covariance[0] = 0 #-1.0

                imu.linear_acceleration.x = float(sensor_data[6])
                imu.linear_acceleration.y = float(sensor_data[7])
                imu.linear_acceleration.z = float(sensor_data[8])

                imu.angular_velocity.x = float(sensor_data[9])
                imu.angular_velocity.y = float(sensor_data[10])
                imu.angular_velocity.z = float(sensor_data[11].split('*')[0])

                mag.header.seq = 0
                mag.header.stamp = rospy.Time.now()
                mag.header.frame_id = 'MAG'
                mag.magnetic_field.x = float(sensor_data[3])
                mag.magnetic_field.y = float(sensor_data[4])
                mag.magnetic_field.z = float(sensor_data[5])
 
                rospy.loginfo(imu)
                pub1.publish(imu)
                pub2.publish(mag)

                
            rate.sleep()
    except rospy.ROSInterruptException:
        rospy.loginfo("SROSInterruptException...")
        port.close()
    
    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down paro_depth node...")


