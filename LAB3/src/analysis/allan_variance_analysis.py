from cProfile import label
from cmath import log, log10, sqrt
from curses.ascii import FS
from enum import unique

import bagpy
from bagpy import bagreader
import matplotlib.pyplot as plt
import numpy as np

import pandas as pd
import math

bag = bagreader('imu_mag_info.bag')
bag.topic_table

data1 = bag.message_by_topic('imu_info')
data2 = bag.message_by_topic('mag_info')

csv_imu = pd.read_csv(data1)
csv_mag = pd.read_csv(data2)

oriW = csv_imu['orientation.w'].values.tolist()
oriX = csv_imu['orientation.x'].values.tolist()
oriY = csv_imu['orientation.y'].values.tolist()
oriZ = csv_imu['orientation.z'].values.tolist()

angX = csv_imu['angular_velocity.x'].values.tolist()
angY = csv_imu['angular_velocity.y'].values.tolist()
angZ = csv_imu['angular_velocity.z'].values.tolist()

accX = csv_imu['linear_acceleration.x'].values.tolist()
accY = csv_imu['linear_acceleration.y'].values.tolist()
accZ = csv_imu['linear_acceleration.z'].values.tolist()

magX = csv_mag['magnetic_field.x'].values.tolist()
magY = csv_mag['magnetic_field.y'].values.tolist()
magZ = csv_mag['magnetic_field.z'].values.tolist()

x = csv_imu['header.seq'].values.tolist()

t0 = 40
theta = np.cumsum(angX) * t0
maxNumM = 100

L = np.size(theta)
maxM = 2**math.floor(math.log2(L/2))
m = np.linalg.inv(np.logspace(log10(1), log10(maxM), maxNumM))
m = math.ceil(float(m))
m = unique(m)

tau = m*t0

avar = np.zeros(np.size(m), 1)
for i in np.size(m):
    mi = m(i)
    avar[i,:] = sum((theta[1+2*mi:L] - 2*theta[1+mi:L-mi] + theta[1:L-2*mi])**2, 1)

avar = avar / (2 * tau**2 * (L -2*m))

adev = sqrt(avar)

plt.figure
plt.loglog(tau, adev)
plt.title('Allan Deviation')
plt.xlabel('\tau')
plt.ylabel('\sigma(\tau)')
plt.grid(1)
plt.axis('equal')

plt.show()

