from cProfile import label
import bagpy
from bagpy import bagreader
from matplotlib import axis
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker
import pandas as pd

bagOfstatic = bagreader('Lab3_Data/imu_info.bag')
bagOfmoving = bagreader('Lab3_Data/mag_info.bag')
bagOfstatic.topic_table
bagOfmoving.topic_table

data1 = bagOfstatic.message_by_topic('imu_info')
data2 = bagOfmoving.message_by_topic('mag_info')
csv_imu = pd.read_csv(data1)
csv_mag = pd.read_csv(data2)

angX = csv_imu['angular_velocity.x'].values.tolist()
angY = csv_imu['angular_velocity.y'].values.tolist()
angZ = csv_imu['angular_velocity.z'].values.tolist()

accX = csv_imu['linear_acceleration.x'].values.tolist()
accY = csv_imu['linear_acceleration.y'].values.tolist()
accZ = csv_imu['linear_acceleration.z'].values.tolist()

magX = csv_mag['magnetic_field.x'].values.tolist()
magY = csv_mag['magnetic_field.y'].values.tolist()
magZ = csv_mag['magnetic_field.z'].values.tolist()

timeX = csv_imu['Time'].values.tolist()
timeXmag = csv_mag['Time'].values.tolist()


print('Mean: %f' %(np.mean(angX)))
print('Standard deviation: %f' %(np.std(angX)))



fig, ax = plt.subplots()
y_avg = [np.mean(angX)] * len(timeX)
plt.figure
ax.plot(timeX, angX, label='Angular rate gyro X')
ax.plot(timeX, y_avg, '-', linewidth=3, color='r', lw = 2, label='average of X')
plt.xlabel("Time")
plt.ylabel("Noise")
plt.legend()
plt.show()

plt.figure
nbins = 16
mu = np.mean(angX)
sigma = np.std(angX)
n, bins, patches = plt.hist(angX, bins = nbins, density = 1, label='Histogram of distribution')
plt.xlabel('[Static] Distribution of gyro X values')
plt.ylabel('frequency')
plt.axvline(x = mu, linewidth = 3, color = 'r', label='Mean value')
plt.text(mu+0.1, 0.06, 'Mean of gyro X', color = 'r')
plt.text(mu+0.8, 0.03, 'Distribution', color = 'orange')
y = ((1 / (np.sqrt(2 * np.pi)* sigma)) * np.exp(-0.5 * (1 / sigma * (bins - mu))**2))
plt.plot(bins, y, '--', color = 'orange', label='Probabilitiy density function')
plt.legend()
plt.show()

y_avg = [np.mean(angY)] * len(timeX)
plt.figure
plt.plot(timeX, angY, label='Angular rate gyro Y')
plt.plot(timeX, y_avg, '-', linewidth=3, color='r', lw = 2, label='average of Y')
plt.xlabel("Time")
plt.ylabel("Noise")
plt.legend()
plt.show()

plt.figure
nbins = 8
mu = np.mean(angY)
sigma = np.std(angY)
n, bins, patches = plt.hist(angY, bins = nbins, density = 1, label='Histogram of distribution')
plt.xlabel('[Static] Distribution of gyro Y values')
plt.ylabel('frequency')
plt.axvline(x = mu, linewidth = 3, color = 'r', label='Mean value')
plt.text(mu+0.1, 0.06, 'Mean of gyro Y', color = 'r')
plt.text(mu+0.8, 0.03, 'Distribution', color = 'orange')
y = ((1 / (np.sqrt(2 * np.pi)* sigma)) * np.exp(-0.5 * (1 / sigma * (bins - mu))**2))
plt.plot(bins, y, '--', color = 'orange', label='Probabilitiy density function')
plt.legend()
plt.show()

y_avg = [np.mean(angZ)] * len(timeX)
plt.figure
plt.plot(timeX, angZ, label='Angular rate gyro Z')
plt.plot(timeX, y_avg, '-', linewidth=3, color='r', lw = 2, label='average of Z')
plt.xlabel("Time")
plt.ylabel("Noise")
plt.legend()
plt.show()

plt.figure
nbins = 8
mu = np.mean(angZ)
sigma = np.std(angZ)
n, bins, patches = plt.hist(angZ, bins = nbins, density = 1, label='Histogram of distribution')
plt.xlabel('[Static] Distribution of gyro Z values')
plt.ylabel('frequency')
plt.axvline(x = mu, linewidth = 3, color = 'r', label='Mean value')
plt.text(mu+0.1, 0.06, 'Mean of gyro Z', color = 'r')
plt.text(mu+0.8, 0.03, 'Distribution', color = 'orange')
y = ((1 / (np.sqrt(2 * np.pi)* sigma)) * np.exp(-0.5 * (1 / sigma * (bins - mu))**2))
plt.plot(bins, y, '--', color = 'orange', label='Probabilitiy density function')
plt.legend()
plt.show()

y_avg = [np.mean(accX)] * len(timeX)
plt.figure
plt.plot(timeX, accX, label='Angular rate accelerometer X')
plt.plot(timeX, y_avg, '-', linewidth=3, color='r', lw = 2, label='average of X')
plt.xlabel("Time")
plt.ylabel("Noise")
plt.legend()
plt.show()

plt.figure
nbins = 8
mu = np.mean(accX)
sigma = np.std(accX)
n, bins, patches = plt.hist(accX, bins = nbins, density = 1, label='Histogram of distribution')
plt.xlabel('[Static] Distribution of accelerometer X values')
plt.ylabel('frequency')
plt.axvline(x = mu, linewidth = 3, color = 'r', label='Mean value')
y = ((1 / (np.sqrt(2 * np.pi)* sigma)) * np.exp(-0.5 * (1 / sigma * (bins - mu))**2))
plt.plot(bins, y, '--', color = 'orange', label='Probabilitiy density function')
plt.legend()
plt.show()

y_avg = [np.mean(accY)] * len(timeX)
plt.figure
plt.plot(timeX, accY, label='Angular rate accelerometer Y')
plt.plot(timeX, y_avg, '-', linewidth=3, color='r', lw = 2, label='average of Y')
plt.xlabel("Time")
plt.ylabel("Noise")
plt.legend()
plt.show()

plt.figure
nbins = 8
mu = np.mean(accY)
sigma = np.std(accY)
n, bins, patches = plt.hist(accY, bins = nbins, density = 1, label='Histogram of distribution')
plt.xlabel('[Static] Distribution of accelerometer Y values')
plt.ylabel('frequency')
plt.axvline(x = mu, linewidth = 3, color = 'r', label='Mean value')
y = ((1 / (np.sqrt(2 * np.pi)* sigma)) * np.exp(-0.5 * (1 / sigma * (bins - mu))**2))
plt.plot(bins, y, '--', color = 'orange', label='Probabilitiy density function')
plt.legend()
plt.show()

y_avg = [np.mean(accZ)] * len(timeX)
plt.figure
plt.plot(timeX, accZ, label='Angular rate accelerometer Z')
plt.plot(timeX, y_avg, '-', linewidth=3, color='r', lw = 2, label='average of Z')
plt.xlabel("Time")
plt.ylabel("Noise")
plt.legend()
plt.show()

plt.figure
nbins = 8
mu = np.mean(accZ)
sigma = np.std(accZ)
n, bins, patches = plt.hist(accZ, bins = nbins, density = 1, label='Histogram of distribution')
plt.xlabel('[Static] Distribution of accelerometer Z values')
plt.ylabel('frequency')
plt.axvline(x = mu, linewidth = 3, color = 'r', label='Mean value')
y = ((1 / (np.sqrt(2 * np.pi)* sigma)) * np.exp(-0.5 * (1 / sigma * (bins - mu))**2))
plt.plot(bins, y, '--', color = 'orange', label='Probabilitiy density function')
plt.legend()
plt.show()

y_avg = [np.mean(magX)] * len(timeXmag)
plt.figure
plt.plot(timeXmag, magX, label='Magnetometer X axis')
plt.plot(timeXmag, y_avg, '-', linewidth=3, color='r', lw = 2, label='average of X')
plt.xlabel("Time")
plt.ylabel("Noise")
plt.legend()
plt.show()

plt.figure
nbins = 8
mu = np.mean(magX)
sigma = np.std(magX)
n, bins, patches = plt.hist(magX, bins = nbins, density = 1, label='Histogram of distribution')
plt.xlabel('[Static] Distribution of magnetometer X values')
plt.ylabel('frequency')
plt.axvline(x = mu, linewidth = 3, color = 'r', label='Mean value')
y = ((1 / (np.sqrt(2 * np.pi)* sigma)) * np.exp(-0.5 * (1 / sigma * (bins - mu))**2))
plt.plot(bins, y, '--', color = 'orange', label='Probabilitiy density function')
plt.legend()
plt.show()

y_avg = [np.mean(magY)] * len(timeXmag)
plt.figure
plt.plot(timeXmag, magY, label='Magnetometer Y axis')
plt.plot(timeXmag, y_avg, '-', linewidth=3, color='r', lw = 2, label='average of Y')
plt.xlabel("Time")
plt.ylabel("Noise")
plt.legend()
plt.show()

plt.figure
nbins = 8
mu = np.mean(magY)
sigma = np.std(magY)
n, bins, patches = plt.hist(magY, bins = nbins, density = 1, label='Histogram of distribution')
plt.xlabel('[Static] Distribution of magnetometer Y values')
plt.ylabel('frequency')
plt.axvline(x = mu, linewidth = 3, color = 'r', label='Mean value')
y = ((1 / (np.sqrt(2 * np.pi)* sigma)) * np.exp(-0.5 * (1 / sigma * (bins - mu))**2))
plt.plot(bins, y, '--', color = 'orange', label='Probabilitiy density function')
plt.legend()
plt.show()

y_avg = [np.mean(magZ)] * len(timeXmag)
plt.figure
plt.plot(timeXmag, magZ, label='Magnetometer Z axis')
plt.plot(timeXmag, y_avg, '-', linewidth=3, color='r', lw = 2, label='average of Z')
plt.xlabel("Time")
plt.ylabel("Noise")
plt.legend()
plt.show()

plt.figure
nbins = 8
mu = np.mean(magZ)
sigma = np.std(magZ)
n, bins, patches = plt.hist(magZ, bins = nbins, density = 1, label='Histogram of distribution')
plt.xlabel('[Static] Distribution of magnetometer Z values')
plt.ylabel('frequency')
plt.axvline(x = mu, linewidth = 3, color = 'r', label='Mean value')
y = ((1 / (np.sqrt(2 * np.pi)* sigma)) * np.exp(-0.5 * (1 / sigma * (bins - mu))**2))
plt.plot(bins, y, '--', color = 'orange', label='Probabilitiy density function')
plt.legend()
plt.show()

