###################################################################
# EECE5554 - LAB2
# Kyungsun Lee / Student ID: 001537130 / lee.kyu@northeastern.edu)
###################################################################

import bagpy
from bagpy import bagreader
import pandas as pd
import seaborn as sea
import matplotlib.pyplot as plt
import numpy as np
import csv
import utm
from scipy import stats
from scipy.stats import mstats
from mpl_toolkits.mplot3d import Axes3D

# read rosbag Data and create csv file.
bag_standing = bagreader('standing_reflection_GNGGA.bag')
bag_standing.topic_table
data = bag_standing.message_by_topic('GPSMsg2')
csv_data_standing = pd.read_csv(data)
gga_string = np.array(csv_data_standing['line'].tolist())

GPS_LATITUDE = []
GPS_LONGITUDE = []

GPS_ALTITUDE = []
GPS_ALTITUDE_FIX_1 = []  # GNSS FIX VALID
GPS_ALTITUDE_FIX_4 = []  # RTK FIXED Ambiguties
GPS_ALTITUDE_FIX_5 = []  # RTK FLOAT Ambiguties

GPS_EASTING = []
GPS_EASTING_FIX_1 = []  # GNSS FIX VALID
GPS_EASTING_FIX_4 = []  # RTK FIXED Ambiguties
GPS_EASTING_FIX_5 = []  # RTK FLOAT Ambiguties

GPS_NORTHING = []
GPS_NORTHING_FIX_1 = []  # GNSS FIX VALID
GPS_NORTHING_FIX_4 = []  # RTK FIXED Ambiguties
GPS_NORTHING_FIX_5 = []  # RTK FLOAT Ambiguties

GPS_ZONE_NUMBER = []
GPS_ZONE_LETTER = []
GPS_QUALITY_INDICATOR = []  # GPS Quality Indicator


# Convert GNGGA String into utm data
for gga_data in gga_string:
    sensorData = gga_data[7:].split(',')
    altitude = 0 if sensorData[8] == '' else float(sensorData[8])
    latitude_dd = int(sensorData[1][:2])
    latitude_mm = float(sensorData[1][2:]) / 60
    longitude_ddd = sensorData[3][:3]
    longitude_mm = float(sensorData[3][3:]) / 60
    if longitude_ddd[0] == '0':
        longitude_ddd = int(longitude_ddd[1:3]) * (-1)
        longitude_mm = longitude_mm * (-1)
    else:
        longitude_ddd = int(longitude_ddd[0:3])
    latitude = latitude_dd + latitude_mm
    longitude = longitude_ddd + longitude_mm
    UTMdata = utm.from_latlon(latitude, longitude)
    GPS_LATITUDE.append(latitude)
    GPS_LONGITUDE.append(longitude)
    GPS_ALTITUDE.append(altitude)
    GPS_ZONE_NUMBER.append(UTMdata[2])
    GPS_ZONE_LETTER.append(UTMdata[3])
    GPS_QUALITY_INDICATOR.append(sensorData[5])
    GPS_EASTING.append(UTMdata[0])
    GPS_NORTHING.append(UTMdata[1])
    if sensorData[5] == '1':
        GPS_EASTING_FIX_1.append(UTMdata[0])
        GPS_NORTHING_FIX_1.append(UTMdata[1])
        GPS_ALTITUDE_FIX_1.append(altitude)
    if sensorData[5] == '4':
        GPS_EASTING_FIX_4.append(UTMdata[0])
        GPS_NORTHING_FIX_4.append(UTMdata[1])
        GPS_ALTITUDE_FIX_4.append(altitude)
    if sensorData[5] == '5':
        GPS_EASTING_FIX_5.append(UTMdata[0])
        GPS_NORTHING_FIX_5.append(UTMdata[1])
        GPS_ALTITUDE_FIX_5.append(altitude)


print("GPS_EASTING_FIX_1")
print(min(GPS_EASTING_FIX_1))
print(max(GPS_EASTING_FIX_1))

print("GPS_EASTING_FIX_4")
print(min(GPS_EASTING_FIX_4))
print(max(GPS_EASTING_FIX_4))

print("GPS_EASTING_FIX_5")
print(min(GPS_EASTING_FIX_5))
print(max(GPS_EASTING_FIX_5))

print("GPS_NORTHING_FIX_1")
print(min(GPS_NORTHING_FIX_1))
print(max(GPS_NORTHING_FIX_1))

print("GPS_NORTHING_FIX_4")
print(min(GPS_NORTHING_FIX_4))
print(max(GPS_NORTHING_FIX_4))

print("GPS_NORTHING_FIX_5")
print(min(GPS_NORTHING_FIX_5))
print(max(GPS_NORTHING_FIX_5))

print("GPS_ALTITUDE_FIX_4")
print(min(GPS_ALTITUDE_FIX_4))
print(max(GPS_ALTITUDE_FIX_4))

print("GPS_ALTITUDE_FIX_1")
print(min(GPS_ALTITUDE_FIX_1))
print(max(GPS_ALTITUDE_FIX_1))

print("GPS_ALTITUDE_FIX_5")
print(min(GPS_ALTITUDE_FIX_5))
print(max(GPS_ALTITUDE_FIX_5))


# Plot 1
plt.plot(GPS_EASTING_FIX_1, GPS_NORTHING_FIX_1,
         color='violet', marker='^', linestyle='--', label='GNSS fix valid')
plt.plot(GPS_EASTING_FIX_5, GPS_NORTHING_FIX_5,
         color='limegreen', marker='o', linestyle='--', label='RTK float Ambiguties')
plt.plot(GPS_EASTING_FIX_4, GPS_NORTHING_FIX_4, color='red',
         marker='o', linestyle='--', label='RTK fixed Ambiguties')
plt.title('UTM Data - Highlights GPS quality indicator')
plt.xlabel('UTM-Easting')
plt.ylabel('UTM-Northing')
plt.legend()
plt.show()


# Plot 2
plt.plot(GPS_EASTING, GPS_NORTHING, color='dodgerblue',
         marker='o', linestyle='--')
plt.title('UTM Data')
plt.xlabel('UTM-Easting')
plt.ylabel('UTM-Northing')
plt.show()


# Plot 3
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(GPS_EASTING_FIX_1, GPS_NORTHING_FIX_1, GPS_ALTITUDE_FIX_1,
           color='violet', label='GNSS fix valid')
ax.scatter(GPS_EASTING_FIX_5, GPS_NORTHING_FIX_5, GPS_ALTITUDE_FIX_5,
           color='limegreen', label='RTK float Ambiguties')
ax.scatter(GPS_EASTING_FIX_4, GPS_NORTHING_FIX_4, GPS_ALTITUDE_FIX_4,
           color='red', label='RTK fixed Ambiguties')
plt.title('UTM Data - Highlights GPS quality indicator')
ax.set_xlabel('UTM-Easting')
ax.set_ylabel('UTM-Northing')
ax.set_zlabel('Altitude')
plt.legend()
plt.show()


# Plot 4
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(GPS_EASTING, GPS_NORTHING, GPS_ALTITUDE)
plt.title('UTM Data - Altitude')
ax.set_xlabel('UTM-Easting')
ax.set_ylabel('UTM-Northing')
ax.set_zlabel('Altitude')
plt.legend()
plt.show()
