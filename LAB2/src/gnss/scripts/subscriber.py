#!/usr/bin/env python
import rospy
from gnss.msg import gnss_msg

def callback(data):
    rospy.loginfo("======= BEGIN =======")
    rospy.loginfo(data)

def subscriber():
    rospy.init_node('subscriber')
    rospy.Subscriber("GPSMsg2", gnss_msg, callback)
    rospy.loginfo("STart! subscribe!")
    rospy.spin()


if __name__ == '__main__':
    subscriber()
