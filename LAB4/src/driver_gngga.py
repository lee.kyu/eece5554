#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import serial
import utm
from math import sin, pi
from std_msgs.msg import Float64
from gnss.msg import gnss_msg

if __name__ == '__main__':
    # Initialization (GPS_driver node, sensor port information)
    rospy.init_node('GPS_driver')
    serial_port = rospy.get_param('~port', '/dev/ttyACM0')
    serial_baud = rospy.get_param('~baudrate', 57600)
    sampling_rate = rospy.get_param('~sampling_rate', 5.0)
    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    rospy.logdebug("Initializing sensor with *0100P4\\r\\n ...")
    sampling_count = int(round(1/(sampling_rate*0.007913)))
    rospy.sleep(0.2)
    line = port.readline()

    # publish GPSMsg
    pub = rospy.Publisher('GPSMsg2', gnss_msg, queue_size=10)
    msg = gnss_msg()

    sleep_time = 1/sampling_rate - 0.025

    try:
        while not rospy.is_shutdown():
            line = port.readline()
            print("kkss")
            line = str(line, 'UTF-8')
            print(line)
            if line == '':
                rospy.loginfo("Sensor: No data")
            else:
                # rospy.loginfo(line)
                # print(line)
                if line.startswith('$GNGGA'):
                    print(line)
                    msg.line = line
                    pub.publish(msg)
            rospy.sleep(sleep_time)

    except rospy.ROSInterruptException:
        port.close()

    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down GPS_driver node...")
