from cProfile import label
from statistics import mean
from traceback import print_exc
from turtle import color
from xml.sax.handler import property_xml_string
from bagpy import bagreader
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from math import pi, atan2
import math
from sympy import Integral, Symbol, symbols
from scipy import integrate
from scipy import signal

# read rosbag Data and create csv file.
lab4_bag = bagreader('lab4_2.bag')
lab4_bag.topic_table

# Retrieve GPS data
gps_data = lab4_bag.message_by_topic('/GPS')
csv_gps_field = pd.read_csv(gps_data)

# Plot GPS trajectory

gps_easting = np.array(csv_gps_field['utm_easting'].tolist())
gps_northing = np.array(csv_gps_field['utm_northing'].tolist())
'''
plt.title('GPS Plot')
plt.plot(gps_easting, gps_northing, label='Trajectory from GPS readings')
plt.xlabel('GPS Easting')
plt.ylabel('GPS Northing')
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()
'''

# Retrieve IMU data
imu_data = lab4_bag.message_by_topic('/IMU')
csv_imu_field = pd.read_csv(imu_data)

# Roll,Pitch,Yaw,Time
imu_time = np.array(csv_imu_field['Time'].tolist())
imu_yaw = np.array(csv_imu_field['Yaw'].tolist())
'''
plt.title('Yaw from IMU Plot')
plt.plot(imu_time, imu_yaw, label='Yaw from IMU readings')
plt.xlabel('Time')
plt.ylabel('Yaw')
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()
'''

# mag.magnetic_field.x,mag.magnetic_field.y,mag.magnetic_field.z
mag_x = np.array(csv_imu_field['mag.magnetic_field.x'].tolist())
mag_y = np.array(csv_imu_field['mag.magnetic_field.y'].tolist())


ellipse_mag_x = mag_x[3500:8000]
ellipse_mag_y = mag_y[3500:8000]

'''
plt.title('[Before/After Correction] Magnetometer readings')
plt.plot(ellipse_mag_x, ellipse_mag_y,
         label='[Before Correction] Magnetometer readings')
plt.xlabel('Magnetic_field.x (gauss)')
plt.ylabel('Magnetic_field.y (gauss)')
'''
# plots center points of the ellipses(before correction).
print("min x,max x")
print(min(ellipse_mag_x))  # 0.0137
print(max(ellipse_mag_x))  # 0.2307

print("min y,max y")
print(min(ellipse_mag_y))  # 0.0574
print(max(ellipse_mag_y))  # 0.2388

center_x = (0.2307 + 0.0137) / 2.0
center_y = (0.2388 + 0.0574) / 2.0

print("center (x, y) : ")
print(center_x, center_y)  # 0.12219999999999999 0.1481

# plt.legend()
# plt.show()

##############################################################################
# Magnetometer Correction
##############################################################################
# plots the ellipse of magnetometer data before the correction.
u = center_x  # x-position of the center
v = center_y  # y-position of the center
radius_x = (max(ellipse_mag_x) - min(ellipse_mag_x)) / 2.0
radius_y = (max(ellipse_mag_y) - min(ellipse_mag_y)) / 2.0
a = radius_x  # radius on the x-axis
b = radius_y  # radius on the y-axis
print("radius_x, radius_y : ")  # 0.1085 0.0907
print(radius_x, radius_y)
'''
t = np.linspace(0, 2*pi, 100)
plt.plot(u+a*np.cos(t), v+b*np.sin(t), color='red', marker='^',
         label='[Before Correction] Best fit')
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()
'''

# hard iron / soft iron correction
hard_soft_iron_correcton_x = []
hard_soft_iron_correcton_y = []
soft_iron_correction_x = radius_y / radius_x
soft_iron_correction_y = radius_x / radius_y
for temp_x in ellipse_mag_x:
    hard_soft_iron_correcton_x.append(
        (temp_x - center_x)*soft_iron_correction_x)

for temp_y in ellipse_mag_y:
    hard_soft_iron_correcton_y.append(
        (temp_y - center_y)*soft_iron_correction_y)

'''
# plots corrected magnetemetor data after hard/soft iron correction
plt.plot(hard_soft_iron_correcton_x, hard_soft_iron_correcton_y,
         label='[After Correction] Magnetometer readings')
plt.xlabel('Magnetic_field.x (gauss)')
plt.ylabel('Magnetic_field.y (gauss)')

# plots the ellipse of magnetometer data after the correction.
u = 0.0  # x-position of the center
v = 0.0  # y-position of the center
a = radius_x * soft_iron_correction_x  # radius on the x-axis
b = radius_y * soft_iron_correction_y  # radius on the y-axis
t = np.linspace(0, 2*pi, 100)
plt.plot(u+a*np.cos(t), v+b*np.sin(t), color='blue', marker='o',
         label='[After Correction] Best fit')
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()
'''


corrected_mag_x = mag_x[9000:]
corrected_mag_y = mag_y[9000:]
rectangle_route_time = imu_time[9000:]

hard_soft_iron_correcton_x = []
hard_soft_iron_correcton_y = []
soft_iron_correction_x = radius_y / radius_x
soft_iron_correction_y = radius_x / radius_y

for temp_x in corrected_mag_x:
    hard_soft_iron_correcton_x.append(
        (temp_x - center_x)*soft_iron_correction_x)

for temp_y in corrected_mag_y:
    hard_soft_iron_correcton_y.append(
        (temp_y - center_y)*soft_iron_correction_y)

rad_yaw_mag = []

for mag_temp_x, mag_temp_y in zip(hard_soft_iron_correcton_x, hard_soft_iron_correcton_y):
    rad_yaw_mag.append(atan2((-1.0) * mag_temp_y, mag_temp_x))


##############################################################################
# Yaw From Gyro [Before filter applition]
##############################################################################
imu_yaw_rectangle = imu_yaw[9000:]
yaw_imu_offset = 59
plt.plot(rectangle_route_time, imu_yaw_rectangle - yaw_imu_offset, color='red',
         label='Yaw from IMU')
gyro_z = np.array(csv_imu_field['data.angular_velocity.z'].tolist())
gyro_z_rectangle = gyro_z[9000:]

gyro_integral = integrate.cumtrapz(gyro_z_rectangle, rectangle_route_time)

integral_time = rectangle_route_time[1:]
plt.plot(integral_time, np.rad2deg(gyro_integral), color='violet',
         label='Yaw from Gyro')

plt.plot(rectangle_route_time, np.rad2deg(rad_yaw_mag),
         label='Yaw from Mag')
plt.xlabel('Timesteps')
plt.ylabel('degree')
plt.grid(color='lightgray', linestyle='--')
plt.title('Yaw Angle (before filter application)')
plt.legend()
plt.show()


yaw_mag_offset = 0.18
plt.plot(rectangle_route_time, np.unwrap(
    rad_yaw_mag) + yaw_mag_offset, label='Yaw from Mag')
plt.plot(rectangle_route_time, np.unwrap(np.deg2rad(imu_yaw_rectangle - yaw_imu_offset)), color='red',
         label='Yaw from IMU')
plt.plot(integral_time, gyro_integral, color='violet', label='Yaw from Gyro')
plt.xlabel('Timesteps')
plt.ylabel('radians')
plt.grid(color='lightgray', linestyle='--')
plt.title('Yaw Angle (before filter application)')
plt.legend()
plt.show()

##############################################################################
# Yaw From Gyro, Mag [After filter applition] ===> Degree
##############################################################################

plt.title('Yaw Angle (After filter application)')
# high pass filter - Gyro estimate
integral_time = rectangle_route_time[1:]
plt.plot(integral_time, np.rad2deg(np.unwrap(gyro_integral)),
         label='Yaw from Gyro (with HPF)')

# low pass filter(unwrap) - MAG estimate
a_wrapped = []
for temp in rad_yaw_mag:
    a_wrapped.append((temp + yaw_mag_offset) % (0.5 * (np.pi)))
plt.plot(rectangle_route_time, np.rad2deg(a_wrapped),
         label='Yaw from Mag (with LPF)', color='green')

# Complementary filter - MAG & Gyro estimate
a_wrapped_cal = a_wrapped[1:]
plt.plot(integral_time, np.rad2deg(a_wrapped_cal) + np.rad2deg(np.unwrap(gyro_integral)),
         label='Yaw (Gyro + Mag combined)', color='orange')

plt.plot(rectangle_route_time, imu_yaw_rectangle - yaw_imu_offset, color='red',
         label='Yaw from IMU')
plt.xlabel('Timesteps')
plt.ylabel('degree')
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()


##############################################################################
# Yaw From Gyro, Mag [After filter applition] ===> Radian
##############################################################################
plt.title('Yaw Angle (After filter application)')
# high pass filter - Gyro estimate
integral_time = rectangle_route_time[1:]
plt.plot(integral_time, np.unwrap(gyro_integral),
         label='Yaw from Gyro (with HPF)')

# low pass filter(unwrap) - MAG estimate
plt.plot(rectangle_route_time, np.unwrap(rad_yaw_mag) + yaw_mag_offset,
         label='Yaw from Mag (with LPF)', color='green')

# Complementary filter - MAG & Gyro estimate
rad_yaw_mag_cal = rad_yaw_mag[1:]
half_gyro_mag_integral = []
for temp, temp_2 in zip(np.unwrap(gyro_integral), np.unwrap(rad_yaw_mag_cal)):
    half_gyro_mag_integral.append(
        (temp * 0.5) + ((temp_2 * 0.5) + yaw_mag_offset))

plt.plot(integral_time, half_gyro_mag_integral,
         label='Yaw (Gyro + Mag combined)', color='orange')

plt.plot(rectangle_route_time, np.unwrap(np.deg2rad(imu_yaw_rectangle - yaw_imu_offset)), color='red',
         label='Yaw from IMU')
plt.xlabel('Timesteps')
plt.ylabel('radians')
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()


# testing (rad to deg)
plt.plot(integral_time, np.rad2deg(np.unwrap(gyro_integral)),
         label='Yaw from Gyro (with HPF)')
plt.plot(rectangle_route_time, np.rad2deg(np.unwrap(rad_yaw_mag) + yaw_mag_offset),
         label='Yaw from Mag (with LPF)', color='green')
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()
##############################################################################
# Yaw From Gyro [After filter applition] *** WEIGHTED ***
##############################################################################
plt.title('Yaw Angle (After weighted filter application)')
plt.plot(integral_time, np.unwrap(gyro_integral),
         label='Yaw from Gyro (with HPF)', linewidth=5)
plt.plot(rectangle_route_time, np.unwrap(rad_yaw_mag) + yaw_mag_offset,
         label='Yaw from Mag (with LPF)', color='green')
weighted = 0.98
half_gyro_mag_integral = []
for temp, temp_2 in zip(np.unwrap(gyro_integral), np.unwrap(rad_yaw_mag_cal)):
    half_gyro_mag_integral.append(
        (temp * weighted) + (temp_2 * (1 - weighted)))

plt.plot(integral_time, half_gyro_mag_integral,
         label='Yaw (Gyro + Mag Weighed)', color='orange')

plt.plot(rectangle_route_time, np.unwrap(np.deg2rad(imu_yaw_rectangle - yaw_imu_offset)), color='red',
         label='Yaw from IMU')

plt.xlabel('Timesteps')
plt.ylabel('radians')
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()


##############################################################################
# Velocity from accel.
##############################################################################

# Velocity from accel.
velocity_time = imu_time[9000:]
accel_x = np.array(csv_imu_field['data.linear_acceleration.x'].tolist())
accel_x_rectangle = accel_x[9000:]
gfg = integrate.cumtrapz(accel_x_rectangle, velocity_time)
velocity_time = velocity_time[1:]
plt.title('Forward velocity estimate')
plt.plot(velocity_time, gfg, label='Velocity from Accelerometer')
plt.plot([1646411212.205655, 1646411878.783424], [0.0019132733345031741,
         42.14367046976038], color="green", label='Bias from the accelerometer data')


# Velocity estimate from GPS
gps_easting = np.array(csv_gps_field['utm_easting'].tolist())
gps_northing = np.array(csv_gps_field['utm_northing'].tolist())
gps_time = np.array(csv_gps_field['Time'].tolist())

gps_easting = gps_easting[220:]
gps_northing = gps_northing[220:]
gps_time = gps_time[220:]
dist = []
pre_easting = gps_easting[0]
pre_northing = gps_northing[0]
velocity_gps = []

for easting, northing in zip(gps_easting, gps_northing):
    dist.append(math.sqrt(math.pow((easting - pre_easting), 2) +
                math.pow((northing - pre_northing), 2)))
    pre_easting = easting
    pre_northing = northing


for dist_en in dist:
    velocity_gps.append(dist_en)

plt.plot(gps_time, velocity_gps, label='Velocity from GPS')

plt.xlabel('Timesteps')
plt.ylabel('Velocity(meter/sec)')
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()


print(len(gfg))

plt.title('Forward velocity estimate (adjusted)')
# adjustments
plt.plot(velocity_time, gfg, label='Velocity from Accelerometer')

adjusted_velocity_gps = []
i = 0
offset_1 = 0.001580474
offset_2 = 0.00215
offset_3 = 0.00155
for adjustments in gfg:
    if i >= 9550 and i < 16700:
        adjusted_velocity_gps.append(adjustments - (offset_2 * i))
    if i >= 16700:
        adjusted_velocity_gps.append(adjustments - (offset_3 * i))
    if i < 9550:
        adjusted_velocity_gps.append(adjustments - (offset_1 * i))
    i = i+1
    # print(i)
    # 26664

plt.plot(velocity_time, adjusted_velocity_gps,
         label='Adjusted Velocity', linewidth='5')
plt.plot(gps_time, velocity_gps, label='Velocity from GPS')
plt.xlabel('Timesteps')
plt.ylabel('Velocity(meter/sec)')
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()


##############################################################################
# 3. X acceleration
##############################################################################
# X acceleration
accel_x = np.array(csv_imu_field['data.linear_acceleration.x'].tolist())
accel_y = np.array(csv_imu_field['data.linear_acceleration.y'].tolist())
imu_time_accel = np.array(csv_imu_field['Time'].tolist())
angular_rate_z = np.array(
    csv_imu_field['data.angular_velocity.z'].tolist())  # rad/s

accel_x = accel_x[9000:]
accel_y = accel_y[9000:]
imu_time_accel = imu_time_accel[9000:]
angular_rate_z = angular_rate_z[9000:]
velocity_x = integrate.cumtrapz(accel_x, imu_time_accel)

estimate_y = []
for temp1, temp2 in zip(angular_rate_z, velocity_x):
    estimate_y.append((temp1 * temp2))

imu_time_accel = imu_time_accel[1:]
accel_y = accel_y[1:]
plt.title('Comparison of Y-direction acceleration')

plt.plot(imu_time_accel, accel_y,
         label='Y-direction acceleration from IMU readings', color='orange')
plt.plot(imu_time_accel, estimate_y,
         label='Estimate of Y-direction acceleration')
plt.xlabel('Timesteps')
plt.ylabel('Acceleration(meter/sec)^2')
plt.ylim(-22, 17)
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()

accel_offset = 0.3
estimate_y = []
for temp1, temp2 in zip(angular_rate_z, velocity_x):
    estimate_y.append((temp1 * temp2) - accel_offset)

plt.title('Comparison of Y-direction acceleration (adjusted)')

plt.plot(imu_time_accel, accel_y,
         label='Y-direction acceleration from IMU readings', color='orange')
plt.plot(imu_time_accel, estimate_y,
         label='Estimate of Y-direction acceleration')
plt.xlabel('Timesteps')
plt.ylabel('Acceleration(meter/sec)^2')
plt.ylim(-22, 17)
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()

##############################################################################
# 3. Trajectory (DR)
##############################################################################
# Trajectory (DR)
trajectory_x = []
trajectory_y = []
pre_x = 0
pre_y = 0

for vel, yaw in zip(adjusted_velocity_gps, np.unwrap(rad_yaw_mag) - yaw_mag_offset):
    trajectory_y.append(pre_y + (vel * np.math.cos(yaw)))
    trajectory_x.append(pre_x + (vel * np.math.sin(yaw)))
    pre_y = pre_y + (vel * np.math.cos(yaw))
    pre_x = pre_x + (vel * np.math.sin(yaw))

scaled_x = []
scaled_y = []

for scale_x, scale_y in zip(trajectory_x, trajectory_y):
    scaled_x.append((scale_x * 0.022) + gps_easting[0])
    scaled_y.append((scale_y * 0.022) + gps_northing[0])


adjusted_x = []
adjusted_y = []

i = 0
for adjust_x, adjust_y in zip(scaled_x, scaled_y):
    if (adjust_x > 327735):
        adjusted_y.append(adjust_y + (0.005 * i))
        adjusted_x.append(adjust_x + (0.009 * i))
    else:
        if (i < 13300):
            adjusted_y.append(adjust_y - (0.015 * i))
            adjusted_x.append(adjust_x)
        else:
            if (i > 24300):
                adjusted_y.append(adjust_y - (0.0025 * i))
                adjusted_x.append(adjust_x + (0.0169 * i))
            else:
                adjusted_y.append(adjust_y - (0.01 * i))
                adjusted_x.append(adjust_x + (0.009 * i))
    i = i+1

plt.title('Trajectory - scale factor: 0.022')

plt.plot(gps_easting, gps_northing, label='GPS data')
#plt.plot(scaled_x, scaled_y, label='Dead Reckoning data (scale factor: 0.022)')
plt.plot(adjusted_x, adjusted_y, label='Dead Reckoning data (adjusted)')
plt.xlabel('easting (meter)')
plt.ylabel('northing (meter)')
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()

##############################################################################
# 3. Estimate Xc
##############################################################################

xc = []
omega = []

for temp in angular_rate_z:
    omega.append(temp * 25)
for temp1, temp2 in zip(estimate_y, omega):
    if (omega != 0.0):
        xc.append(temp1/temp2*(-1))

plt.title('Estimate xc')
plt.plot(xc, label='estimated xc')
plt.grid(color='lightgray', linestyle='--')
plt.legend()
plt.show()

print(mean(xc))
