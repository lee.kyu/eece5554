#!/usr/bin/env python3

import rospy
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField

def callback_imu(data):
    rospy.loginfo(data)

def callback_mag(data):
    rospy.loginfo(data)

def listener():

    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('imu_info', Imu, callback_imu)
    rospy.Subscriber('mag_info', MagneticField, callback_mag)
    rospy.spin()

if __name__ == '__main__':
    listener()

